
# Activity:
# 1. Create a Class called Camper and give it the attributes name, batch, course_type
# 2. Create a method called career_track which will print out the string Currently enrolled in the <value of course_type> program.
# 3. Create a method called info which will print out the string My name is <value of name> of batch <value of batch>.
# 4. Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type.
# 5. Print the value of the object's name, batch, course type.
# 6. Execute the info method and career_track of the object.

class Camper():
    def __init__(self, name, batch, course_type):
        
        # creating properties
        self.name = name
        self.batch = batch
        self.course_type = course_type

    # method
    def career_track(self):
        print(f"Currently enrolled in {self.course_type}")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}")

        
zuitt_camper = Camper("Ronie", "246", "python short course program")

print(f"Name: {zuitt_camper.name}\nBatch: {zuitt_camper.batch}\nCourse: {zuitt_camper.course_type}")

zuitt_camper.info()
zuitt_camper.career_track()


